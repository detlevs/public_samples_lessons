﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 20.05.2012
 * Time: 10:14
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.IO;

using log4net;

namespace DatabaseHelper
{
	/// <summary>
	/// Description of DatabaseManager.
	/// </summary>
	public class DatabaseManager
	{
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(DatabaseManager));
				
		public const string PATH_SAMPLEDB = @"..\..\..\..\SampleDB";
		/// <summary>
		/// The default database Path.
		/// </summary>
		public const string PATH_DB_DEFAULT = @"DB\";
		
		public const string PATH_DB = PATH_DB_DEFAULT + @"testdatabase.db";
		/// <summary>
		/// Default connection String.
		/// </summary>
		public const string CONNECTION_STRING_DEFAULT = @"Data Source=" + PATH_DB + ";Version=3;New=True;";
		
		public const string SQL_SCRIPT = @"\Chinook_Sqlite.sql";

		public const string SQLITE_DATABASE_PREPARED = @"\Chinook_Sqlite.sqlite";		
		
		/// <summary>
		/// Create test database.
		/// </summary>
		/// <returns>true if database is created. otherwise false.</returns>
		public static bool CopyDatabase()
		{
			try
			{
				CreateDatabaseFolder();
				
				if(File.Exists(PATH_DB))
				{
					DropDatabase();
				}
				
				File.Copy(PATH_SAMPLEDB + SQLITE_DATABASE_PREPARED, PATH_DB);
			}
			catch(Exception ex)
			{
				log.Error(ex);
				return false;
			}
			return true;
		}
		
		/// <summary>
		/// Create test database.
		/// </summary>
		/// <returns>true if database is created. otherwise false.</returns>
		public static bool CreateDatabase()
		{
			log.Info("Create test datase.");
			try
			{
				CreateDatabaseFolder();
				CreateSchema();
				return true;
			}
			catch(Exception ex)
			{
				log.Error(ex);
			}
			return false;
		}
		
		/// <summary>
		/// Drop test database.
		/// </summary>
		/// <returns>true if database is droped. otherwise false.</returns>
		public static bool DropDatabase()
		{
			try
			{
				File.Delete(PATH_DB);
			}
			catch(Exception ex)
			{
				log.Error(ex);
				return false;
			}
			return true;
		}
		
		/// <summary>
		/// CreateDatabaseFolder
		/// </summary>
		private static void CreateDatabaseFolder()
		{
			System.IO.Directory.CreateDirectory(PATH_DB_DEFAULT);
		}
		
		/// <summary>
		/// Create Schema on Database.
		/// </summary>
		private static void CreateSchema()
	    {
			StreamReader script = new StreamReader(SQL_SCRIPT);
			
			//new SchemaExport(conf).Create(true, true);
			SQLiteConnection.CreateFile(PATH_DB);
			
			SqlConnection con = new SqlConnection(CONNECTION_STRING_DEFAULT);
			con.Open();
			
			using(SqlTransaction tx = con.BeginTransaction())
			{
				

				tx.Commit();
			}
			
			con.Close();
		}
	}
}
