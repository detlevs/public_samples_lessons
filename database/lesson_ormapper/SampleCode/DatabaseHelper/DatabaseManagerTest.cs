﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 20.05.2012
 * Time: 13:04
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using log4net;
using NUnit.Framework;

namespace DatabaseHelper
{
	/// <summary>
	/// Description of DatabaseManagerTest.
	/// </summary>
	[TestFixture]
	public class DatabaseManagerTest
	{
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(DatabaseManagerTest));
		
		public DatabaseManagerTest()
		{
		}
		#region SetUp / TearDown
		[TestFixtureSetUp]
		public void Init()
		{
			log.Debug("TestFixtureSetUp");
		}
		
		[TestFixtureTearDown]
		public void Dispose()
		{
			log.Debug("TestFixtureTearDown");
		}
		
		[SetUp]
		public void Setup()
		{
		}
		#endregion 
		
		#region Test Repoitory Base
		[Test]
		public void CopyDataBase()
		{
			Assert.IsTrue(DatabaseManager.CopyDatabase());
		}
		
//		[Test]
//		public void CreateDataBase()
//		{
//			Assert.IsTrue(DatabaseManager.CreateDatabase());
//		}
		
				[Test]
		public void DropDataBase()
		{
			Assert.IsTrue(DatabaseManager.CopyDatabase());
			Assert.IsTrue(DatabaseManager.DropDatabase());
		}
		#endregion
	}
}
