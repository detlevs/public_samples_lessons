﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 24.05.2012
 * Time: 20:28
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using NHibernate;
using NHibernateMappingByCode.Domain;

namespace NHibernateMappingByCode.Repository
{
	/// <summary>
	/// Description of IArtistRepository.
	/// </summary>
	public interface IArtistRepository : IRepository<Artist>
	{
		IList<Artist> GetByAlbum(string albumTitel);
		
		IList<Artist> GetByAlbumLike(string albumTitel);
		
		IList<Artist> GetByAlbumAll(string albumTitel);
		
		IList<Artist> GetByAlbumJoin(string albumTitel);
		
		IList<Artist> GetByName(string name);
	}
}
