﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 24.05.2012
 * Time: 20:28
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;

using log4net;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernateMappingByCode.Domain;
using NHibernateMappingByCode.Domain.Mapping;

namespace NHibernateMappingByCode.Repository
{
	/// <summary>
	/// Description of ArtistRepository.
	/// </summary>
	public class ArtistRepository : Repository<Artist>, IArtistRepository
	{
		#region Logging
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(ArtistRepository));
		#endregion
		
		#region Constructor
		public ArtistRepository(ISessionFactory sessionFactory) : base (sessionFactory)
		{
		}
		#endregion
			
		#region IArtistRepository		
		public IList<Artist> GetByAlbumJoin(string albumTitel)
		{
			
			//    /* criteria query */ SELECT
			//        this_.ArtistId as ArtistId1_1_,
			//        this_.Name as Name1_1_,
			//        album1_.AlbumId as AlbumId0_0_,
			//        album1_.Title as Title0_0_,
			//        album1_.ArtistId as ArtistId0_0_ 
			//    FROM
			//        Artist this_ 
			//    inner join
			//        Album album1_ 
			//            on this_.ArtistId=album1_.ArtistId 
			//    WHERE
			//        album1_.Title = @p0;
			//    @p0 = 'Out Of Exile' [Type: String (0)]
			
			IList<Artist> resultList = null;
			using(ISession session = sessionFactory.OpenSession())
			{
				using (ITransaction tx = session.BeginTransaction())
				{
					try
					{
						resultList =
							session.QueryOver<Artist>()
							.JoinQueryOver<Album>(ar => ar.Albums)
							.Where(ar => ar.Title == albumTitel)
							.List<Artist>();
						tx.Commit();
					}
					catch(HibernateException ex)
					{
						log.Error(ex);
						tx.Rollback();
					}
				}
			}
			return resultList;
		}
		
		public IList<Artist> GetByAlbumLike(string albumTitel)
		{
			IList<Artist> resultList = null;
			
			if(string.IsNullOrEmpty(albumTitel))
			{
				resultList = new List<Artist>();
			}
			else
			{
				using(ISession session = sessionFactory.OpenSession())
				{
					using (ITransaction tx = session.BeginTransaction())
					{
						try
						{
							// Projection is need to define the Artist ID as result of the subquery
							// that will be used for IN
							resultList =  session.CreateCriteria(typeof(Artist))
								.SetFlushMode(FlushMode.Never)
								.SetReadOnly(true)
								.Add(Restrictions.Like("Name", albumTitel + "%"))
								.List<Artist>();
							tx.Commit();
						}
						catch(HibernateException ex)
						{
							log.Error(ex);
							tx.Rollback();
						}
					}
				}
			}
			return resultList;
		}
		
		public IList<Artist> GetByAlbum(string albumTitel)
		{			
			IList<Artist> resultList = null;
			using(ISession session = sessionFactory.OpenSession())
			{
				using (ITransaction tx = session.BeginTransaction())
				{
					try
					{
						// Projection is need to define the Artist ID as result of the subquery
						// that will be used for IN
						resultList =  session.CreateCriteria(typeof(Artist))
							.SetFlushMode(FlushMode.Never)
							.SetReadOnly(true)
							.Add(Subqueries.PropertyIn("Id",
							                           DetachedCriteria.For<Album>()
							                           .Add(Restrictions.Eq("Title", albumTitel))
							                           .SetProjection(Projections.Property("Artist.Id"))
							                          ))
							.List<Artist>();
						tx.Commit();
					}
					catch(HibernateException ex)
					{
						log.Error(ex);
						tx.Rollback();
					}
				}
			}			
			return resultList;
		}
		
		public IList<Artist> GetByAlbumAll(string albumTitel)
		{
			IList<Artist> resultList = null;
			if(string.IsNullOrEmpty(albumTitel))
			{
				resultList = new List<Artist>();
			}
			else
			{
				using(ISession session = sessionFactory.OpenSession())
				{
					using (ITransaction tx = session.BeginTransaction())
					{
						try
						{
							resultList = 
								session.CreateCriteria<Artist>()
								.CreateCriteria("Albums", JoinType.LeftOuterJoin)
								.Add(Expression.Eq(Album.TITLE, albumTitel))
								.SetFetchMode("Albums", FetchMode.Join)
								//.SetFetchSize(100)
								.List<Artist>();
							tx.Commit();
						}
						catch(HibernateException ex)
						{
							log.Error(ex);
							tx.Rollback();
						}
					}
				}	
			}
			return resultList;
		}
		
		public IList<Artist> GetByName(string name)
		{
			IList<Artist> resultList = null;
			if(string.IsNullOrEmpty(name))
			{
				resultList = new List<Artist>();
			}
			else
			{
				using(ISession session = sessionFactory.OpenSession())
				{
					using (ITransaction tx = session.BeginTransaction())
					{
						try
						{
							resultList = 
								session.CreateCriteria<Artist>()
								.Add(Expression.Eq(Artist.NAME, name))
								.List<Artist>();
							tx.Commit();
						}
						catch(HibernateException ex)
						{
							log.Error(ex);
							tx.Rollback();
						}
					}
				}	
			}
			return resultList;
		}
		#endregion
	}
}
