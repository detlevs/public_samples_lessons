﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 01.06.2012
 * Time: 21:04
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NHibernateMappingByCode.Domain;

namespace NHibernateMappingByCode.Repository
{
	/// <summary>
	/// Description of IAlbumRepository.
	/// </summary>
	public interface IAlbumRepository : IRepository<Album>
	{
		
	}
}
