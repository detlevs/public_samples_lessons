﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 20.02.2010
 * Time: 18:47
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;

using log4net;
using NHibernate;
using NHibernate.Connection;

namespace NHibernateMappingByCode.Repository
{
	/// <summary>
	/// Implementation of Interface IRepository.
	/// Provide common Method to access Domain Object.
	/// </summary>
	public abstract partial class Repository<T> : IRepository<T>
	{
		#region Logging
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(Repository<T>));
		#endregion		
		
		#region Protected member
		/// <summary>
		/// Database Session Factory
		/// </summary>
		protected ISessionFactory sessionFactory;
		#endregion

		#region Constructor
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="connectionUrl">Connection URL</param>
		public Repository(string connectionUrl) 
		{
			log.Debug("TODO: to we need to set connection string ?");
	    }
		
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="sessionFactory">Database Session Factory</param>
		public Repository(ISessionFactory sessionFactory) 
		{
			this.sessionFactory = sessionFactory;
	    }
		#endregion		

		/// <summary>
		/// Add a Domain Item.
		/// </summary>
		/// <param name="item">Domain Item.</param>
		/// <returns>Returns the ID (Identifier) of the generated Item. On Errors it returns NULL.</returns>
		public object Add(T item)
		{
			log.Debug("Item:" + item.ToString());
			object ret = null;
			using(ISession session = this.sessionFactory.OpenSession())
			{
				using (ITransaction transaction = session.BeginTransaction())
				{
					try
					{
						// save return the PK
						ret = session.Save(item);
						transaction.Commit();
					}
					catch(HibernateException ex)
					{
						log.Error(ex);
						transaction.Rollback();
					}
				}
			}
			return ret;
		}
		
		/// <summary>
		/// Update a Domain Item.
		/// </summary>
		/// <param name="item">Domain Item.</param>
		public void Update(T item)
		{
			log.Debug("Item:" + item.ToString());
			using(ISession session = this.sessionFactory.OpenSession())
			{
				using (ITransaction transaction = session.BeginTransaction())
				{
					try
					{
						session.SaveOrUpdate(item);
						transaction.Commit();
					}
					catch(HibernateException ex)
					{
						log.Error(ex);
						transaction.Rollback();
					}
				}
			}
		}
		
		/// <summary>
		/// Save a Domain Item.
		/// </summary>
		/// <param name="item">Domain Item.</param>
		public void Save(T item)
		{
			using(ISession session = this.sessionFactory.OpenSession())
			{
				using (ITransaction transaction = session.BeginTransaction())
				{
					try
					{						 
						session.SaveOrUpdate(item);
						transaction.Commit();
					}
					catch(HibernateException ex)
					{
						log.Error(ex);
						transaction.Rollback();
					}
				}
			}
		}
				
		/// <summary>
        /// remove item from database.
        /// </summary>
        /// <param name="key">Key of Domain Item.</param>
        public void Remove(object key)
        {
        	int id;
        	try
        	{
        		id = Convert.ToInt32(key);
        	}
        	catch(Exception ex)
        	{
        		log.Warn("Parameter is not a key.", ex);
        		return;
        	}
        	
			using(ISession session = this.sessionFactory.OpenSession())
			{
				using (ITransaction transaction = session.BeginTransaction())
				{
					try
					{
						T aa = session.Get<T>(id);
						session.Delete(aa);
					
						transaction.Commit();
					}
					catch(HibernateException ex)
					{
						log.Error(ex);
						transaction.Rollback();
					}
				}
			}
		}
        
		/// <summary>
		/// Remove a Domain Item.
		/// </summary>
		/// <param name="item">Domain Item.</param>
		public void Remove(T item)
		{
			log.Debug("Item:" + item.ToString());
			using(ISession session = this.sessionFactory.OpenSession())
			{
				using (ITransaction transaction = session.BeginTransaction())
				{
					try
					{
						session.Delete(item);
						transaction.Commit();
					}
					catch(HibernateException ex)
					{
						log.Error(ex);
						transaction.Rollback();
					}
				}
			}
		}
		
		/// <summary>
		/// Get a Domain Item by Key.
		/// </summary>
		/// <param name="itemId">Key Object.</param>
		/// <returns></returns>
		public T GetByKey(object key)
		{
			T item = default(T);
			using(ISession session = this.sessionFactory.OpenSession())
			{
				using (ITransaction transaction = session.BeginTransaction())
				{
					try
					{
						item = (T)session.Get(typeof(T), key);
						transaction.Commit();
					}
					catch(HibernateException ex)
					{
						log.Error(ex.Message);
						transaction.Rollback();
					}
				}
			}
			return item;
		}
		
		/// <summary>
        /// Check, if a Domain Item with given key exists.
        /// </summary>
        /// <param name="id">Key Object</param>
        /// <returns>true, if Item exits.</returns>
		public bool Exists(object id)
		{
			object obj = null;
			using(ISession session = this.sessionFactory.OpenSession())
			{
				using (ITransaction transaction = session.BeginTransaction())
				{
					try
					{
//						obj = GetById(id);
//						Type type = typeof(Repository<T>); // is correct !
//						log.Debug(type);
						obj = session.Get<T>(id);
						//obj = session.Get(typeof(Repository<T>), id);
						transaction.Commit();
					} 
					catch(HibernateException ex)
					{
						log.Error(ex.Message);
						transaction.Rollback();
					}
				}
			}
			return (obj != null);
		}
		
		/// <summary>
        /// Get All Entries.
        /// </summary>
        /// <returns>List of all Entreis.</returns>
		public IList<T> FindAll()
		{
			IList<T> list = new List<T>();
			using(ISession session = this.sessionFactory.OpenSession())
			{
				using (ITransaction transaction = session.BeginTransaction())
				{
					try
					{
						// Cast<T> comes from Linq : using System.Linq;
						list = session.CreateCriteria(typeof(T)).List().Cast<T>().ToList();
						transaction.Commit();
					}
					catch(HibernateException ex)
					{
						log.Error(ex.Message);
						transaction.Rollback();
					}
				}
			}
			return list;
		}
		
	}
}
