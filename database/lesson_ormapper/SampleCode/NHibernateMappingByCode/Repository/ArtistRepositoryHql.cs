﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 03.06.2012
 * Time: 10:19
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using NHibernateMappingByCode.Domain;

namespace NHibernateMappingByCode.Repository
{
	/// <summary>
	/// Description of ArtistRepositoryHql.
	/// </summary>
	public class ArtistRepositoryHql : Repository<Artist>, IArtistRepository
	{
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(ArtistRepositoryHql));
		
		public ArtistRepositoryHql(ISessionFactory sessionFactory) : base (sessionFactory)
		{
		}
		
		#region IArtistRepository
		
		public IList<Artist> GetByAlbumJoin(string albumTitel)
		{
			
			//    /* criteria query */ SELECT
			//        this_.ArtistId as ArtistId1_1_,
			//        this_.Name as Name1_1_,
			//        album1_.AlbumId as AlbumId0_0_,
			//        album1_.Title as Title0_0_,
			//        album1_.ArtistId as ArtistId0_0_ 
			//    FROM
			//        Artist this_ 
			//    inner join
			//        Album album1_ 
			//            on this_.ArtistId=album1_.ArtistId 
			//    WHERE
			//        album1_.Title = @p0;
			//    @p0 = 'Out Of Exile' [Type: String (0)]
			
			IList<Artist> resultList = null;
			using(ISession session = sessionFactory.OpenSession())
			{
				using (ITransaction tx = session.BeginTransaction())
				{
					try
					{
						// The HQL join uses the defined relation in Album 
						resultList =  session.CreateQuery("FROM Artist ar INNER JOIN ar.Albums al WHERE al.Title=:title")
							.SetFlushMode(FlushMode.Never)
							.SetReadOnly(true)
							.SetParameter("title", albumTitel)
							.List<Artist>();
						tx.Commit();
					}
					catch(HibernateException ex)
					{
						log.Error(ex);
						tx.Rollback();
					}
				}
			}
			return resultList;
		}
		
		public IList<Artist> GetByAlbumLike(string albumTitel)
		{
			IList<Artist> resultList = null;
			using(ISession session = sessionFactory.OpenSession())
			{
				using (ITransaction tx = session.BeginTransaction())
				{
					try
					{					
						// Projection is need to define the Artist ID as result of the subquery
						// that will be used for IN
						resultList =  session.CreateQuery("FROM Artist WHERE Name LIKE ':title%'")
											.SetFlushMode(FlushMode.Never)
											.SetReadOnly(true)
											.SetParameter("title", albumTitel)
											.List<Artist>();
						tx.Commit();
					}
					catch(HibernateException ex)
					{
						log.Error(ex);
						tx.Rollback();
					}
				}
			}
			return resultList;
		}
		
		public IList<Artist> GetByAlbum(string albumTitel)
		{
//						/* criteria query */ SELECT
//					        this_.ArtistId as ArtistId1_0_,
//					        this_.Name as Name1_0_ 
//					    FROM
//					        Artist this_ 
//					    WHERE
//					        this_.ArtistId in (
//					            /* criteria query */ SELECT
//					                this_0_.ArtistId as y0_ 
//					            FROM
//					                Album this_0_ 
//					            WHERE
//					                this_0_.Title = @p0
//					        );
//					    @p0 = 'Let There Be Rock' [Type: String (0)]

			IList<Artist> resultList = null;
			using(ISession session = sessionFactory.OpenSession())
			{
				using (ITransaction tx = session.BeginTransaction())
				{
					try
					{					
						// Projection is need to define the Artist ID as result of the subquery
						// that will be used for IN
						resultList =  session.CreateQuery("FROM Artist ar WHERE ar.Id IN (SELECT al.Id FROM Album al WHERE al.Title=:title)")
							.SetFlushMode(FlushMode.Never)
							.SetReadOnly(true)
							.SetParameter("title", albumTitel)
							.List<Artist>();
						tx.Commit();
					}
					catch(HibernateException ex)
					{
						log.Error(ex);
						tx.Rollback();
					}
				}
			}
			return resultList;
		}
		
		public IList<Artist> GetByAlbumAll(string albumTitel)
		{
			IList<Artist> resultList = null;
			using(ISession session = sessionFactory.OpenSession())
			{
				using (ITransaction tx = session.BeginTransaction())
				{
					try
					{					
						// Projection is need to define the Artist ID as result of the subquery
						// that will be used for IN
						resultList =  session.CreateQuery("FROM Artist ar LEFT OUTER JOIN Album al WHERE al.Title=:title)")
							.SetFlushMode(FlushMode.Never)
							.SetReadOnly(true)
							.SetParameter("title", albumTitel)
							.List<Artist>();
						tx.Commit();
					}
					catch(HibernateException ex)
					{
						log.Error(ex);
						tx.Rollback();
					}
				}
			}
			return resultList;
		}
		
		public IList<Artist> GetByName(string name)
		{
			IList<Artist> resultList = null;
			if(string.IsNullOrEmpty(name))
			{
				resultList = new List<Artist>();
			}
			else
			{
				using(ISession session = sessionFactory.OpenSession())
				{
					using (ITransaction tx = session.BeginTransaction())
					{
						try
						{
							resultList =  session.CreateQuery("FROM Artist WHERE al.Name=:name)")
							.SetFlushMode(FlushMode.Never)
							.SetReadOnly(true)
							.SetParameter("name", name)
							.List<Artist>();
							tx.Commit();
						}
						catch(HibernateException ex)
						{
							log.Error(ex);
							tx.Rollback();
						}
					}
				}	
			}
			return resultList;
		}
		#endregion
	}
}
