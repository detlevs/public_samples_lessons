﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 20.02.2010
 * Time: 18:36
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using NHibernate;

namespace NHibernateMappingByCode.Repository
{
	/// <summary>
	/// The Interface IRepository provides the declaration of basic 
	/// database access methods.
	/// </summary>
	public interface IRepository<T>
	{		
		/// <summary>
		/// Add item to database,
		/// </summary>
		/// <param name="item">Data object.</param>
		/// <returns>Returns the ID (Identifier) of the generated Item. On Errors it returns NULL.</returns>
		object Add(T item);

		/// <summary>
		/// Update item in database.
		/// </summary>
		/// <param name="item">Data object.</param>
        void Update(T item);
        
        /// <summary>
		/// Update item in database.
		/// </summary>
		/// <param name="item">Data object.</param>
        void Save(T item);

        /// <summary>
        /// remove item from database.
        /// </summary>
        /// <param name="item">Data object.</param>
        void Remove(T item);
        
        /// <summary>
        /// remove item from database.
        /// </summary>
        /// <param name="key">Key of Domain Item.</param>
        void Remove(object key);
        
        /// <summary>
        /// Check, if a Domain Item with given key exists.
        /// </summary>
        /// <param name="id">Key Object</param>
        /// <returns>true, if Item exits.</returns>
        bool Exists(object id);

        /// <summary>
        /// Get item from database by key.
        /// </summary>
        /// <param name="itemId">Primary Key ID</param>
        /// <returns></returns>
        T GetByKey(object key);
        
        /// <summary>
        /// Get All Entries.
        /// </summary>
        /// <returns>List of all Entries.</returns>
        IList<T> FindAll();
	}
}
