﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 01.06.2012
 * Time: 21:04
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using log4net;
using NHibernate;
using NHibernateMappingByCode.Domain;

namespace NHibernateMappingByCode.Repository
{
	/// <summary>
	/// Description of AlbumRepository.
	/// </summary>
	public class AlbumRepository : Repository<Album>, IAlbumRepository
	{
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(AlbumRepository));
		
		public AlbumRepository(ISessionFactory sessionFactory) : base (sessionFactory)
		{
		}
	}
}
