﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 02.06.2012
 * Time: 11:56
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Reflection;

using NHibernate;
using NHibernate.Metadata;
using NHibernate.Persister.Entity;

namespace NHibernateMappingByCode.Helper
{
	/// <summary>
	/// Description of NHibernateHelper.
	/// 
	/// Source:
	/// http://stackoverflow.com/questions/753732/how-to-enumerate-column-names-with-nhibernate
	/// http://stackoverflow.com/questions/1800930/getting-class-field-names-and-table-column-names-from-nhibernate-metadata/8425996#8425996
	/// 
	/// </summary>
	public class NHibernateHelper
	{
		public static string[] GetPropertyAndColumnNames2(ISessionFactory sessionFactory, object domainClass)
		{
			
			if(domainClass == null)
			{
				return null;
			}			
			
			// get an instance to the metadata 
			IClassMetadata metadata = sessionFactory.GetClassMetadata(domainClass.GetType());

			return metadata.PropertyNames;
			
//			// use properties and methods from the metadata:
//			// metadata.PropertyNames
//			// metadata.PropertyTypes
//			// metadata.GetIdentifier()
//			// and more
//			
//			// or get the metadata for all classes at once
//			IDictionary allClassMetaData = factory.GetAllClassMetadata();
//			metadata = allClassMetaData[typeof(entity)];
			
		}
		
		
		/// <summary>
        /// Creates a dictionary of property and database column/field name given an
        /// NHibernate mapped entity
        /// </summary>
        /// <remarks>
        /// This method uses reflection to obtain an NHibernate internal private dictionary.
        /// This is the easiest method I know that will also work with entitys that have mapped components.
        /// </remarks>
        /// <param name="sessionFactory">NHibernate SessionFactory</param>
        /// <param name="entity">An mapped entity</param>
        /// <returns>Entity Property/Database column dictionary</returns>
        public static Dictionary<string, string> GetPropertyAndColumnNames(ISessionFactory sessionFactory, object entity)
        {
            // Get the objects type
            Type type = entity.GetType();

            // Get the entity's NHibernate metadata
            var metaData = sessionFactory.GetClassMetadata(type.ToString());

            // Gets the entity's persister
            var persister = (AbstractEntityPersister)metaData;

            // Creating our own Dictionary<Entity property name, Database column/filed name>()
            var d = new Dictionary<string, string>();

            // Get the entity's identifier
            string entityIdentifier = metaData.IdentifierPropertyName;

            // Get the database identifier
            // Note: We are only getting the first key column.
            // Adjust this code to your needs if you are using composite keys!
            string databaseIdentifier = persister.KeyColumnNames[0];

            // Adding the identifier as the first entry
            d.Add(entityIdentifier, databaseIdentifier);

            // Using reflection to get a private field on the AbstractEntityPersister class
            var fieldInfo = typeof(AbstractEntityPersister)
                .GetField("subclassPropertyColumnNames", BindingFlags.NonPublic | BindingFlags.Instance);

            // This internal NHibernate dictionary contains the entity property name as a key and
            // database column/field name as the value
            var pairs = (Dictionary<string, string[]>)fieldInfo.GetValue(persister);

            foreach (var pair in pairs)
            {
                if (pair.Value.Length > 0)
                {
                    // The database identifier typically appears more than once in the NHibernate dictionary
                    // so we are just filtering it out since we have already added it to our own dictionary
                    if (pair.Value[0] == databaseIdentifier)
                        break;

                    d.Add(pair.Key, pair.Value[0]);
                }
            }

            return d;
        }

		/// <summary>
        /// Gets the list of database column names for an entity
        /// </summary>
        /// <param name="sessionFactory">NHibernate SessionFactory</param>
        /// <param name="entity">A mapped entity</param>
        /// <returns>List of column names</returns>
        public static List<string> GetPropertyColumnNames(ISessionFactory sessionFactory, object entity)
        {
            Type type = entity.GetType();

            // This has some cool methods and properties so check it out
            var metaData = sessionFactory.GetClassMetadata(type.ToString());

            // This has some even cooler methods and properties so definitely check this out
            var entityPersister = (AbstractEntityPersister)metaData;

            // Get the entity's identifier
            string entityIdentifier = metaData.IdentifierPropertyName;

            // Get the database identifier
            // Note: We are only getting the first key column.
            // Adjust this code to your needs if you are using composite keys!
            string databaseIdentifier = entityPersister.KeyColumnNames[0];

            var propertyNames = entityPersister.PropertyNames;

            List<string> columnNames = new List<string>();

            // Adding the database identifier first
            columnNames.Add( databaseIdentifier );

            foreach (var propertyName in propertyNames)
            {
                var columnNameArray = entityPersister.GetPropertyColumnNames(propertyName);

                foreach (var column in columnNameArray)
                {
                    // Filtering out junk
                    if (column != databaseIdentifier)
                    {
                        columnNames.Add(column);
                    }
                }
            }

            return columnNames;
        }
	}
}
