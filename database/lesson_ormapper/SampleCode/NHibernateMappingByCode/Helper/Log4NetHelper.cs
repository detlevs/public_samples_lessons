﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 31.05.2012
 * Time: 22:39
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using log4net.Config;

namespace NHibernateMappingByCode.Helper
{
	/// <summary>
	/// Description of Log4NetHelper.
	/// </summary>
	public class Log4NetHelper
	{
		/// <summary>
		/// 
		/// </summary>
		private Log4NetHelper(){}
		
		/// <summary>
		/// Load log4net configuration.
		/// </summary>
		public static void LoadLoggingConfig() 
		{
			BasicConfigurator.Configure();
			XmlConfigurator.Configure(new FileInfo("log4net.xml"));
			System.Diagnostics.Debug.WriteLine("log4net Logging is enabled.");
		}
	}
}
