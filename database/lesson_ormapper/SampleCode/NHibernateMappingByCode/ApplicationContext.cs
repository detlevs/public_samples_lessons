﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 18.05.2012
 * Time: 23:33
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Data;
using System.Reflection;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;
using NHibernateMappingByCode.Domain;
using NHibernateMappingByCode.Domain.Mapping;

namespace NHibernateMappingByCode
{
	/// <summary>
	/// Description of ApplicationContext.
	/// </summary>
	public class ApplicationContext
	{
		public static Configuration NHConfiguration { get; set; }
	    public static ISessionFactory SessionFactory { get; set; }
	    	
	    public static void AppConfigure()
	    {
	      NHConfiguration = ConfigureNHibernate();
	      SessionFactory = NHConfiguration.BuildSessionFactory();
	    }
	
	    private static Configuration ConfigureNHibernate()
	    {	    	
	      var configure = new Configuration();
	      configure.SessionFactoryName("NHibernate.Test");
	      string connectionString = "Data Source=DB/testdatabase.db;Version=3;";	
	      configure.DataBaseIntegration(db =>
	      {
	        db.Dialect<SQLiteDialect>();
	        db.Driver<SQLite20Driver>();
	        db.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote;
	        db.IsolationLevel = IsolationLevel.ReadCommitted;
	        db.ConnectionString = connectionString;
	        db.Timeout = 10;
	        // enabled for test
	        db.LogFormattedSql = true;
	        db.LogSqlInConsole = true;
	        db.AutoCommentSql = true;
	      });
	
	      var mapping = GetMappings();
	      configure.AddDeserializedMapping(mapping, null);
	
	      return configure;
	    }
	
	    private static HbmMapping GetMappings()
	    {
	      var mapper = new ModelMapper();	
	      
	      // add all classes in same assembly and namespace as class 'Artist'
	      mapper.AddMappings(Assembly.GetAssembly(typeof(Artist)).GetExportedTypes());
	      // add all classes in same assembly and namespace as class 'ArtistMap'
	      mapper.AddMappings(Assembly.GetAssembly(typeof(ArtistMap)).GetExportedTypes());
	      var mapping = mapper.CompileMappingForAllExplicitlyAddedEntities();
	      return mapping;
	    }
	}
}
