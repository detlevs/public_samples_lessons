﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 23.05.2012
 * Time: 22:53
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace NHibernateMappingByCode.Domain.Mapping
{
	/// <summary>
	/// Description of ArtistMap.
	/// </summary>
	public class ArtistMap : ClassMapping<Artist>
	{
		public ArtistMap()
		{
			Table("Artist");

			Id(p=>p.Id, map=>
			   {
			   	map.Column("ArtistId");
			   	map.Generator(Generators.Identity);
			   });
			
			Property(p=>p.Name, map=>
			         {
			         	map.Column("Name");
			         	map.NotNullable(true);
			         	map.Length(120);
			         });
			
			/// How to reference from Artist to Album.
			/// 1:n Relation to Album
			/// ArtistId is the reference hold in this Table
			Bag(p=>p.Albums, map=>
			    {
			    	map.Cascade(Cascade.None);
			    	map.Key(k=>k.Column("ArtistId"));
			    	// load referenced domains while query
			    	//map.Lazy(CollectionLazy.NoLazy);
			    }
			    , ce=>ce.OneToMany());
		}
	}
}
