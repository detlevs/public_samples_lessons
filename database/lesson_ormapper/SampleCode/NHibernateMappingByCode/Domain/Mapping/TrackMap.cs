﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 29.05.2012
 * Time: 20:10
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;

namespace NHibernateMappingByCode.Domain.Mapping
{
	/// <summary>
	/// Description of Track.
	/// </summary>
	public class TrackMap  : ClassMapping<Track>
	{
		public TrackMap()
		{
			Table("Track");
			Id(p=>p.Id, map=>
			   {
			   	map.Column("TrackId");
			   	map.Generator(Generators.Assigned);
			   });
			
			Property(p=>p.Composer, map=>
			         {
			         	map.Column("Composer");
			         	map.NotNullable(true);			         	
			         	map.Length(220);
			         });
			
			Property(p=>p.Milliseconds, map=>
			         {
			         	map.Column("Milliseconds");
			         });
			
			Property(p=>p.Name, map=>
			         {
			         	map.Column("Name");
			         	map.NotNullable(true);
			         	map.Length(200);
			         });
			
			Property(p=>p.Bytes, map=>
			         {
			         	map.Column("Bytes");
			         });
			
			Property(p=>p.UnitPrice, map=>
			         {
			         	map.Column("UnitPrice");
			         	map.Precision(10);
			         	map.Scale(2);
			         });
			
			/// n:1 Relation to Album
			ManyToOne(p=>p.Album, map=>
			          {
			          	map.Column("AlbumId");
			          	map.NotNullable(true);
			          	map.Cascade(Cascade.None);
			          });
			
			/// n:1 Relation to Genre
			ManyToOne(p=>p.Genre, map=>
			          {
			          	map.Column("GenreId");
			          	map.NotNullable(true);
			          	map.Cascade(Cascade.None);
			          });
			
			/// n:1 Relation to MadiaType
			ManyToOne(p=>p.MediaType, map=>
			          {
			          	map.Column("MediaTypeId");
			          	map.NotNullable(true);
			          	map.Cascade(Cascade.None);
			          });
		}
	}
}
