﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 22.09.2012
 * Time: 15:42
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace NHibernateMappingByCode.Domain.Mapping
{
	/// <summary>
	/// Description of ArtistNameAndAlbumTitleMap.
	/// </summary>
	public class ArtistNameAndAlbumTitleMap : ClassMapping<ArtistNameAndAlbumTitle>
	{
		public ArtistNameAndAlbumTitleMap()
		{			
			Table("ArtistNameAndAlbumTitle");
			Property(p=>p.ArtistName, map=>
			         {
			         	map.Column("ArtistName");
			         	map.NotNullable(true);
			         	map.Length(120);
			         });
			Property(p=>p.AlbumTitle, map=>
			         {
			         	map.Column("AlbumTitle");
			         	map.NotNullable(true);
			         	map.Length(160);
			         });
		}
	}
}
