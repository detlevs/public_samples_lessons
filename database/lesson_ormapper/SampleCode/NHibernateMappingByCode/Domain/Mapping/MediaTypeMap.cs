﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 29.05.2012
 * Time: 20:09
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace NHibernateMappingByCode.Domain.Mapping
{
	/// <summary>
	/// Description of MediaTypeMap.
	/// </summary>
	public class MediaTypeMap : ClassMapping<MediaType>
	{
		public MediaTypeMap()
		{
			Table("MediaType");
			Id(p=>p.Id, map=>
			   {
			   	map.Column("MediaTypeId");
			   	map.Generator(Generators.Assigned);
			   });
			
			Property(p=>p.Name, map=>
			         {
			         	map.Column("Name");
			         	map.NotNullable(true);
			         	map.Length(120);
			         });
			
			/// 1:n Relation to Tracks
			Bag(p=>p.Tracks, map=>
			    {
			    	map.Cascade(Cascade.None);
			    	map.Key(k=>k.Column("MediaTypeId"));
			    }
			    , ce=>ce.OneToMany());
		}
	}
}
