﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 29.05.2012
 * Time: 20:10
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace NHibernateMappingByCode.Domain.Mapping
{
	/// <summary>
	/// Description of Genre.
	/// </summary>
	public class GenreMap : ClassMapping<Genre>
	{
		public GenreMap()
		{
			Table("Genre");
			Id(p=>p.Id, map=>
			   {
			   	map.Column("GenreId");
			   	map.Generator(Generators.Assigned);
			   });
			
			Property(p=>p.Name, map=>
			         {
			         	map.Column("Name");
			         	map.NotNullable(true);
			         	map.Length(120);
			         });
			
			/// 1:n Relation to Tracks
			Bag(p=>p.Tracks, map=>
			    {
			    	map.Cascade(Cascade.None);
			    	map.Key(k=>k.Column("GenreId"));
			    }
			    , ce=>ce.OneToMany());
		}
	}
}
