﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 23.09.2012
 * Time: 09:00
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace NHibernateMappingByCode.Domain.Mapping
{
	/// <summary>
	/// Description of ArtistNameAndAlbumTitleIdMap.
	/// </summary>
	public class ArtistNameAndAlbumTitleIdMap : ClassMapping<ArtistNameAndAlbumTitleId>
	{
		public ArtistNameAndAlbumTitleIdMap()
		{
			Table("ArtistNameAndAlbumTitleId");
			
			ComposedId(cid=>{
			           	cid.Property(x=>x.AlbumId);
			           	cid.Property(x=>x.ArtistId);
			           });
			
			Property(p=>p.ArtistId, map=>
			         {
			         	map.Column("ArtistId");
			         });
			Property(p=>p.AlbumId, map=>
			         {
			         	map.Column("AlbumId");
			         });
			
			Property(p=>p.ArtistName, map=>
			         {
			         	map.Column("ArtistName");
			         	map.NotNullable(true);
			         	map.Length(120);
			         });
			Property(p=>p.AlbumTitle, map=>
			         {
			         	map.Column("AlbumTitle");
			         	map.NotNullable(true);
			         	map.Length(160);
			         });
		}
	}
}
