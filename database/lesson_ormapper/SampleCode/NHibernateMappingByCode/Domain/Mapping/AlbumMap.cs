﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 23.05.2012
 * Time: 22:53
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace NHibernateMappingByCode.Domain.Mapping
{
	/// <summary>
	/// Description of AlbumMap.
	/// </summary>
	public class AlbumMap : ClassMapping<Album>
	{
		public AlbumMap()
		{
			Table("Album");
			
			Id(p=>p.Id, map=>
			   {
			   	map.Column("AlbumId");
			   	map.Generator(Generators.Assigned);
			   });
		
			Property(p=>p.Title, map=>
			         {
			         	map.Column("Title");
			         	map.NotNullable(true);
			         	map.Length(160);
			         });
			
			/// n:1 Relation to Artist
			/// ArtistId is comun of foreign table
			ManyToOne(p=>p.Artist, map=>
			          {
			          	//map.ForeignKey("ArtistId");
			            map.Column("ArtistId");
			          	map.NotNullable(true);
			          	map.Cascade(Cascade.None);
			          	// load referenced domains while query
			          	//map.Lazy(LazyRelation.Proxy);
			          });
		}
	}
}
