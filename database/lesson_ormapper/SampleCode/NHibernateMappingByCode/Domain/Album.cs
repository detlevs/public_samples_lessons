﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 23.05.2012
 * Time: 22:37
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Text;
using log4net;

namespace NHibernateMappingByCode.Domain
{
	/// <summary>
	/// Description of Album.
	/// </summary>
	public class Album
	{
		#region Logging
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(Album));
		#endregion
		
		#region Column names
		public static string ID = "Id";
		public static string TITLE = "Title";
		#endregion
		
		#region Constructor
		public Album()
		{
		}
		
		public Album(Artist artist)
		{
			Artist = artist;
		}
		#endregion
			
		#region Properties
		
		public virtual int Id { get; set; }
		public virtual string Title { get; set; }
		
		#endregion
		
		#region Properties References
		
		/// <summary>
		/// Foreign Key Artist.ArtistId
		/// </summary>
		public virtual Artist Artist { get; set; }
		
		#endregion
		
		#region override
		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			if( obj is Album){
				Album item = obj as Album;
				if( this.Id.Equals(item.Id) &&
					this.Title.Equals(item.Title) &&
					this.Artist.Equals(item.Artist)) 
				{
					return true;
				}
				return false;
			}
			return base.Equals(obj);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			StringBuilder buf = new StringBuilder();
			buf.Append(" AlbumId=");
			buf.Append(Id);
			buf.Append(" Title=");
			buf.Append(Title);
			buf.Append(" Artist=");
			buf.Append(Artist);			
			return buf.ToString();
		}
		
		#endregion
	}
}
