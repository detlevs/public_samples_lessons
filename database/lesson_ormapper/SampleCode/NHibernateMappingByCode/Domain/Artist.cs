﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 23.05.2012
 * Time: 22:37
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;

using log4net;

namespace NHibernateMappingByCode.Domain
{
	/// <summary>
	/// Description of Artist.
	/// </summary>
	public class Artist	{
		
		#region Logging
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(Artist));
		#endregion
		
		#region Column names
		public static string ID = "Id";
		public static string NAME = "Name";
		#endregion
		
		#region Properties		
		public virtual int Id { get; set; }
		public virtual string Name { get; set; }		
		#endregion
		
		#region Constructor
		
		public Artist(){}
		
		#endregion
		
		#region Properties References
		public virtual IList<Album> Albums { get; set; }
		#endregion
		
		#region override
		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			if( obj is Artist){
				Artist item = obj as Artist;
				if( this.Id.Equals(item.Id) &&
					this.Name.Equals(item.Name)) 
				{
					return true;
				}
				return false;
			}
			return base.Equals(obj);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			StringBuilder buf = new StringBuilder();
			buf.Append(" ArtistId=");
			buf.Append(Id);
			buf.Append(" Name=");
			buf.Append(Name);
			
			return buf.ToString();
		}
		#endregion
	}
}
