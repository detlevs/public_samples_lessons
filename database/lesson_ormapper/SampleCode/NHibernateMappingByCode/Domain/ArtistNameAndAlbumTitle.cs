﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 22.09.2012
 * Time: 15:36
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Text;
using log4net;

namespace NHibernateMappingByCode.Domain
{
	/// <summary>
	/// Description of ArtistNameAndAlbumTitle.
	/// </summary>
	public class ArtistNameAndAlbumTitle
	{
		#region Logging
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(ArtistNameAndAlbumTitle));
		#endregion
		
		#region Column names
		public static string ARTISTNAME = "ArtistName";
		public static string ALBUMNAME = "AlbumTitle";
		#endregion		
		
		#region Properties		
		public virtual string ArtistName { get; set; }
		public virtual string AlbumTitle { get; set; }		
		#endregion
		
		#region Constructor
		public ArtistNameAndAlbumTitle()
		{
		}
		#endregion
		
		#region override
		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			if( obj is ArtistNameAndAlbumTitle){
				ArtistNameAndAlbumTitle item = obj as ArtistNameAndAlbumTitle;
				if( this.AlbumTitle.Equals(item.AlbumTitle) &&
					this.ArtistName.Equals(item.ArtistName)) 
				{
					return true;
				}
				return false;
			}
			return base.Equals(obj);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return (ArtistName + AlbumTitle).GetHashCode();
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			StringBuilder buf = new StringBuilder();
			buf.Append(" ArtistName=");
			buf.Append(ArtistName);
			buf.Append(" AlbumTitle=");
			buf.Append(AlbumTitle);
			
			return buf.ToString();
		}
		#endregion
	}
}
