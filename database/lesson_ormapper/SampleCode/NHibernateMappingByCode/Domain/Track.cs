﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 29.05.2012
 * Time: 19:57
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Text;
using log4net;

namespace NHibernateMappingByCode.Domain
{
	/// <summary>
	/// Description of Track.
	/// </summary>
	public class Track
	{
		#region Logging
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(Track));
		#endregion
		
		#region Column names
		public static string ID = "Id";
		public static string NAME = "Name";
		public static string COMPOSER = "Composer";
		public static string MILLISECONDS = "Milliseconds";
		public static string BYTES = "Bytes";
		public static string UNITPRICE = "UnitPrice";
		#endregion
		
		#region Constructor
		
		public Track()
		{
		}
		
		#endregion
		
		#region Properties
		
		public virtual int Id { get; set; }
		public virtual string Name { get; set; }		
		public virtual string Composer { get; set; }
		public virtual int Milliseconds { get; set; }
		public virtual int Bytes { get; set; }
		public virtual decimal UnitPrice { get; set; }
		
		#endregion
		
		#region Properties References
		
	    /// <summary>
	    /// [AlbumId] INTEGER,
	    /// </summary>
	    public virtual Album Album { get; set; }
	    
	    /// <summary>
	    /// [MediaTypeId] INTEGER  NOT NULL,
	    /// </summary>
	    public virtual MediaType MediaType { get; set; }
	    
	    /// <summary>
	    /// [GenreId] INTEGER,
	    /// </summary>
		public virtual Genre Genre { get; set; }
		#endregion
		
		#region override
		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			if( obj is Artist){
				Track item = obj as Track;
				if( this.Id.Equals(item.Id) &&
					this.Name.Equals(item.Name) &&
					this.Composer.Equals(item.Composer) &&
					this.UnitPrice.Equals(item.UnitPrice) &&
					this.Bytes.Equals(item.Bytes) &&
					this.Milliseconds.Equals(item.Milliseconds)) 
				{
					return true;
				}
				return false;
			}
			return base.Equals(obj);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			StringBuilder buf = new StringBuilder();
			buf.Append(" Id=");
			buf.Append(Id);
			buf.Append(" Name=");
			buf.Append(Name);
			buf.Append(" Composer=");
			buf.Append(Composer);
			buf.Append(" UnitPrice=");
			buf.Append(UnitPrice);
			buf.Append(" Milliseconds=");
			buf.Append(Milliseconds);
			buf.Append(" Bytes=");
			buf.Append(Bytes);
			
			
			return buf.ToString();
		}
		#endregion
	}
}
