﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 23.09.2012
 * Time: 08:56
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Text;
using log4net;

namespace NHibernateMappingByCode.Domain
{
	/// <summary>
	/// Description of ArtistNameAndAlbumTitleId.
	/// </summary>
	public class ArtistNameAndAlbumTitleId
	{
		#region Logging
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(ArtistNameAndAlbumTitleId));
		#endregion
		
		#region Column names
		public static string ARTISTID = "ArtistId";
		public static string ALBUMID = "AlbumId";
		public static string ARTISTNAME = "ArtistName";
		public static string ALBUMNAME = "AlbumTitle";
		#endregion		
		
		#region Properties		
		public virtual int ArtistId { get; set; }
		public virtual int AlbumId { get; set; }
		public virtual string ArtistName { get; set; }
		public virtual string AlbumTitle { get; set; }		
		#endregion
		
		#region Constructor
		public ArtistNameAndAlbumTitleId()
		{
		}
		#endregion
		
		#region override
		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			if( obj is ArtistNameAndAlbumTitleId){
				ArtistNameAndAlbumTitleId item = obj as ArtistNameAndAlbumTitleId;
				if( this.ArtistId.Equals(item.ArtistId) &&
					this.AlbumId.Equals(item.AlbumId) &&
					this.AlbumTitle.Equals(item.AlbumTitle) &&
					this.ArtistName.Equals(item.ArtistName)) 
				{
					return true;
				}
				return false;
			}
			return base.Equals(obj);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return (ArtistName + AlbumTitle).GetHashCode();
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			StringBuilder buf = new StringBuilder();
			buf.Append(" ArtistId=");
			buf.Append(ArtistId);
			buf.Append(" AlbumId=");
			buf.Append(AlbumId);
			buf.Append(" ArtistName=");
			buf.Append(ArtistName);
			buf.Append(" AlbumTitle=");
			buf.Append(AlbumTitle);
			
			return buf.ToString();
		}
		#endregion
	}
}
