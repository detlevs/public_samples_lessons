﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 24.05.2012
 * Time: 20:54
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using NHibernateMappingByCode.Helper;
using NHibernateMappingByCodeTest.Helper;
using NUnit.Framework;

namespace NHibernateMappingByCodeTest
{
	/// <summary>
	/// Description of SetupTests.
	/// </summary>
	[SetUpFixture]
	public class SetupTests
	{
		[SetUp]
		public void RunBeforeAnyTests()
		{
		  // ...
//		  Console.SetOut(new CustomDebugWriter());
//		  Console.Out.WriteLine("Run Unit Tests ......");
		  
		  Log4NetHelper.LoadLoggingConfig();
		}
	
	    [TearDown]
		public void RunAfterAnyTests()
		{
		  // ...
		}
	}
}
