﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 22.09.2012
 * Time: 16:45
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using DatabaseHelper;
using log4net;
using NHibernate;
using NHibernateMappingByCode;
using NUnit.Framework;

namespace NHibernateMappingByCodeTest
{
	/// <summary>
	/// Description of UnitTestDomainBase.
	/// </summary>
	abstract public class UnitTestDomainBase
	{
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(UnitTestDomainBase));
		
		/// <summary>
		/// Database Session 
		/// </summary>
		protected ISessionFactory sessionFactory;
		
		public UnitTestDomainBase()
		{
		}
		
		#region SetUp / TearDown
		[TestFixtureSetUp]
		virtual public void Init()
		{
			//Log4NetHelper.LoadLoggingConfig();
			log.Debug("TestFixtureSetUp");
			
			Assert.IsTrue(DatabaseManager.CopyDatabase());
			
			ApplicationContext.AppConfigure();

			// Use Real DB
			sessionFactory = ApplicationContext.SessionFactory;
			Assert.IsNotNull(sessionFactory);
		}
		
		[TestFixtureTearDown]
		virtual public void Dispose()
		{
			log.Debug("TestFixtureTearDown");
			Assert.IsTrue(DatabaseManager.DropDatabase());
		}
		
		[SetUp]
		virtual public void Setup()
		{
		}
		
		[TearDown]
		virtual public void TearDown()
		{
		}
		#endregion
	}
}
