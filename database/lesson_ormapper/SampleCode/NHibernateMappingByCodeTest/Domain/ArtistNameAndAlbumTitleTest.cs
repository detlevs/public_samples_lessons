﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 22.09.2012
 * Time: 16:44
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using log4net;
using NHibernate;
using NHibernate.Transform;
using NHibernateMappingByCode.Domain;
using NUnit.Framework;

namespace NHibernateMappingByCodeTest.Domain
{
	/// <summary>
	/// Description of ArtistNameAndAlbumTitleTest.
	/// </summary>
	public class ArtistNameAndAlbumTitleTest : UnitTestDomainBase
	{
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(ArtistNameAndAlbumTitleTest));
		
		#region Test Repoitory Base
		[Test]
		public void GetAll()
		{
			using(ISession session = sessionFactory.OpenSession())
			{
				using (ITransaction tx = session.BeginTransaction())
				{
					IList<ArtistNameAndAlbumTitle> resultList = null;
					try
					{
						// HINT: We have to use Alias here. NHibernate needs this to map column names from SQL query to class properties.
						resultList = session.CreateQuery("select at.ArtistName as ArtistName, at.AlbumTitle as AlbumTitle from ArtistNameAndAlbumTitle at")
							.SetResultTransformer(Transformers.AliasToBean(typeof(ArtistNameAndAlbumTitle)))
							.List<ArtistNameAndAlbumTitle>();
						
						Assert.IsNotNull(resultList);
						Assert.AreEqual(347, resultList.Count);
						tx.Commit();
					}
					catch(HibernateException ex)
					{
						log.Error(ex);
						tx.Rollback();
						Assert.Fail(ex.Message);
					}
				}
			}
		}
		#endregion
	}
}
