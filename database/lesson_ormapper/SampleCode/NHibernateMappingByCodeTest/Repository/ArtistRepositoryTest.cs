﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 22.04.2012
 * Time: 11:20
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;

using log4net;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Hql.Util;
using NHibernate.SqlCommand;
using NHibernateMappingByCode.Domain;
using NHibernateMappingByCode.Repository;
using NUnit.Framework;

namespace NHibernateMappingByCodeTest.Repository
{
	/// <summary>
	/// Description of AuthorRepositoryTest.
	/// </summary>
	[TestFixture]
	public class ArtistRepositoryTest : UnitTestBase
	{
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(ArtistRepositoryTest));
		
		/// <summary>
		/// 
		/// </summary>
		private IArtistRepository repository;
		
		#region SetUp / TearDown
		[TestFixtureSetUp]
		public override void Init()
		{
			base.Init();
			
			repository = base.repositoryArtist;
			Assert.IsNotNull(repository);
		}
		#endregion 
		
		#region Test Repoitory Base
		[Test]
		public void GetById()
		{
			{
				Artist a = repository.GetByKey(1);
				Assert.IsNotNull(a);
				Assert.AreEqual("AC/DC", a.Name);
				
				Assert.IsNotNull(a.Albums);
				//Assert.AreEqual(2, a.Albums.Count);
			}
			
			// this ID does not exists
			{
				Artist a = repository.GetByKey(0);
				Assert.IsNull(a);
			}
		}
		
		[Test]
		public void Exists()
		{			
			// ID = 16 exists
			bool exists = repository.Exists(1);
			Assert.IsTrue(exists);
			
			// ID = 16 dosen't exists
			exists = repository.Exists(9999);
			Assert.IsFalse(exists);
		}
		
		[Test]
		public void FindAll()
		{	
			IList<Artist> items = repository.FindAll();
			Assert.IsTrue(items.Count > 0);
			Assert.AreEqual(275, items.Count);
		}
		
		[Test]
		public void Remove_ByKey()
		{	
			int countBefore = repositoryArtist.FindAll().Count;
			Artist a = new Artist();
			a.Name = "xxx";
			
			repositoryArtist.Save(a);
			Assert.AreNotEqual(0, a.Id);
			Assert.AreEqual(countBefore + 1, repositoryArtist.FindAll().Count);
			
			repository.Remove(a.Id);
			Assert.AreEqual(countBefore, repositoryArtist.FindAll().Count);
		}
		
		[Test]
		public void Remove_ByItem()
		{	
			int countBefore = repositoryArtist.FindAll().Count;
			Artist a = new Artist();
			a.Name = "xxx";
			
			repositoryArtist.Save(a);
			Assert.AreEqual(countBefore + 1, repositoryArtist.FindAll().Count);
			
			repository.Remove(a);
			Assert.AreEqual(countBefore, repositoryArtist.FindAll().Count);
		}
		
		[Test]
		public void Save() 
		{
			// simple save
			{
				int countBefore = repository.FindAll().Count;
				
				Artist a = new Artist();
				a.Name = "xxx";
				repository.Save(a);
				
				int countAfter = repository.FindAll().Count;
				
				Assert.IsTrue(countAfter > countBefore);
				IList<Artist> aa = repository.GetByName(a.Name);
				
				Assert.AreEqual(1, aa.Count);
				Assert.AreEqual(a, aa[0]);
			}
			
		}
		
		#endregion
	
		#region Test IArtistRepository
		
		[Test]
		public void GetByAlbumAll()
		{
			// NULL Parameter
			{					
				IList<Artist> items = repository.GetByAlbumAll(null);
				Assert.IsNotNull(items);
				Assert.AreEqual(0, items.ToList<Artist>().Count);
			}
			
			// empty Parameter
			{					
				IList<Artist> items = repository.GetByAlbumAll(string.Empty);
				Assert.IsNotNull(items);
				Assert.AreEqual(0, items.ToList<Artist>().Count);
			}
			
			// regular parameter
			{
				string albumTitle = "Let There Be Rock";
				IList<Artist> items = repository.GetByAlbumAll(albumTitle);
				Assert.IsNotNull(items);
				Assert.AreEqual(1, items.ToList<Artist>().Count);
				
				//Assert.AreEqual("AC/DC", items[0].Name);
				foreach(var artist in items)
				{
					Assert.IsNotNull(artist);
					Assert.AreEqual(1, artist.Id);
					Assert.AreEqual(1, artist.Albums.Count);
					log.Info(artist);
					
					foreach(var album in artist.Albums)
					{
						Assert.IsNotNull(album);
						Assert.AreEqual(4, album.Id);
						log.Info(album);
					}
				}
			}
		}
			
		[Test]
		public void GetByAlbumLike()
		{
			// NULL Parameter
			{					
				IList<Artist> items = repository.GetByAlbumLike(null);
				Assert.IsNotNull(items);
				Assert.AreEqual(0, items.Count);
			}
			
			// like A%
			{					
				IList<Artist> items = repository.GetByAlbumLike("A");
				Assert.IsNotNull(items);
				Assert.AreEqual(26, items.Count);
			}
		}
		
		[Test]
		public void GetByAlbum()
		{	
			// NULL Parameter
			{					
				IList<Artist> items = repository.GetByAlbum(null);
				Assert.IsNotNull(items);
				Assert.AreEqual(0, items.Count);
			}
			
			// empty Parameter
			{					
				IList<Artist> items = repository.GetByAlbum(string.Empty);
				Assert.IsNotNull(items);
				Assert.AreEqual(0, items.Count);
			}
			
			// regular parameter
			{
				string albumTitle = "Let There Be Rock";
				IList<Artist> items = repository.GetByAlbum(albumTitle);
				Assert.IsNotNull(items);
				Assert.AreEqual(1, items.Count);
				
				Assert.AreEqual("AC/DC", items[0].Name);
			}
			
			// regular parameter
			{
				string albumTitle = "Out Of Exile";
				IList<Artist> items = repository.GetByAlbum(albumTitle);
				Assert.IsNotNull(items);
				Assert.AreEqual(1, items.Count);
				
				Assert.AreEqual("Audioslave", items[0].Name);
			}
		}
				
		[Test]
		public void GetByAlbumJoin()
		{	
			// NULL Parameter
			{
				IList<Artist> items = repository.GetByAlbumJoin(null);
				Assert.IsNotNull(items);
				Assert.AreEqual(0, items.Count);	
			}
			
			// regular parameter
			{
				string albumTitle = "Let There Be Rock";
				IList<Artist> items = repository.GetByAlbumJoin(albumTitle);
				Assert.IsNotNull(items);
				Assert.AreEqual(1, items.Count);
				
				Assert.AreEqual("AC/DC", items[0].Name);
			}
			
			// regular parameter
			{
				string albumTitle = "Out Of Exile";
				IList<Artist> items = repository.GetByAlbumJoin(albumTitle);
				Assert.IsNotNull(items);
				Assert.AreEqual(1, items.Count);
				
				Assert.AreEqual("Audioslave", items[0].Name);
			}
		}
		#endregion
		
		#region Sample of wrong query 
		
		[Test]
		public void SampleWrongQuery()
		{	
			using(ISession session = sessionFactory.OpenSession())
			{
				using (ITransaction tx = session.BeginTransaction())
				{
					try
					{
						IList<Artist> resultList = 
								session.CreateCriteria<Artist>()
								.CreateCriteria("Albums", JoinType.LeftOuterJoin)
								.Add(Expression.Eq(Album.TITLE, 2)) // a string is expected
								.SetFetchMode("Albums", FetchMode.Join)
								.List<Artist>();
						tx.Commit();
					}
					catch(HibernateException ex)
					{
						log.Error(ex);
						tx.Rollback();
					}
				}
			}
		}
		#endregion
	}
}
