﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 03.06.2012
 * Time: 10:22
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using log4net;
using NHibernateMappingByCode.Domain;
using NHibernateMappingByCode.Repository;
using NUnit.Framework;

namespace NHibernateMappingByCodeTest.Repository
{
	/// <summary>
	/// Description of ArtistRepositoryHqlTest.
	/// </summary>
	[TestFixture]
	public class ArtistRepositoryHqlTest : UnitTestBase
	{
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(ArtistRepositoryHqlTest));
				
		/// <summary>
		/// 
		/// </summary>
		private IArtistRepository repository;
		
		#region SetUp / TearDown
		[TestFixtureSetUp]
		public override void Init()
		{
			base.Init();
			
			repository = base.repositoryArtist;
			Assert.IsNotNull(repository);
		}
		
		[TestFixtureTearDown]
		public override void Dispose()
		{
			base.Dispose();
		}
		
		[SetUp]
		public override void Setup()
		{
			base.Setup();
		}
		#endregion 
		
		#region Test IArtistRepository
		
		[Test]
		public void GetByAlbumAll()
		{
			// NULL Parameter
			{					
				IList<Artist> items = repository.GetByAlbumAll(null);
				Assert.IsNotNull(items);
				Assert.AreEqual(0, items.Count);
			}
			
			// empty Parameter
			{					
				IList<Artist> items = repository.GetByAlbumAll(string.Empty);
				Assert.IsNotNull(items);
				Assert.AreEqual(0, items.Count);
			}
			
			// regular parameter
			{
				string albumTitle = "Let There Be Rock";
				IList<Artist> items = repository.GetByAlbumAll(albumTitle);
				Assert.IsNotNull(items);
				Assert.AreEqual(1, items.Count);
				
				foreach(var artist in items)
				{
					Assert.IsNotNull(artist);
					Assert.AreEqual(1, artist.Id);
					Assert.AreEqual(1, artist.Albums.Count);
					log.Info(artist);
					
					foreach(var album in artist.Albums)
					{
						Assert.IsNotNull(album);
						Assert.AreEqual(4, album.Id);
						log.Info(album);
					}
				}
			}
		}
				
		[Test]
		public void GetByAlbum()
		{	
			// NULL Parameter
			{					
				IList<Artist> items = repository.GetByAlbum(null);
				Assert.IsNotNull(items);
				Assert.AreEqual(0, items.Count);
			}
			
			// emptyParameter
			{					
				IList<Artist> items = repository.GetByAlbum(string.Empty);
				Assert.IsNotNull(items);
				Assert.AreEqual(0, items.Count);
			}
			
			// regular parameter
			{
				string albumTitle = "Let There Be Rock";
				IList<Artist> items = repository.GetByAlbum(albumTitle);
				Assert.IsNotNull(items);
				Assert.AreEqual(1, items.Count);
				
				Assert.AreEqual("AC/DC", items[0].Name);
			}
			
			// regular parameter
			{
				string albumTitle = "Out Of Exile";
				IList<Artist> items = repository.GetByAlbum(albumTitle);
				Assert.IsNotNull(items);
				Assert.AreEqual(1, items.Count);
				
				Assert.AreEqual("Audioslave", items[0].Name);
			}
		}
				
		[Test]
		public void GetByAlbumJoin()
		{	
			// NULL Parameter
			{
				IList<Artist> items = repository.GetByAlbumJoin(null);
				Assert.IsNotNull(items);
				Assert.AreEqual(0, items.Count);	
			}
			
			// regular parameter
			{
				string albumTitle = "Let There Be Rock";
				IList<Artist> items = repository.GetByAlbumJoin(albumTitle);
				Assert.IsNotNull(items);
				Assert.AreEqual(1, items.Count);
				
				Assert.AreEqual("AC/DC", items[0].Name);
			}
			
			// regular parameter
			{
				string albumTitle = "Out Of Exile";
				IList<Artist> items = repository.GetByAlbumJoin(albumTitle);
				Assert.IsNotNull(items);
				Assert.AreEqual(1, items.Count);
				
				Assert.AreEqual("Audioslave", items[0].Name);
			}
		}
		#endregion
		
	}
}
