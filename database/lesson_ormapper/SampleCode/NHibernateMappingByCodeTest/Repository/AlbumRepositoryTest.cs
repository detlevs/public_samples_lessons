﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 05.06.2012
 * Time: 21:20
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using log4net;
using NHibernateMappingByCode.Repository;
using NUnit.Framework;

namespace NHibernateMappingByCodeTest.Repository
{
	/// <summary>
	/// Description of AlbumRepositoryTest.
	/// </summary>
	[TestFixture]
	public class AlbumRepositoryTest : UnitTestBase
	{
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(AlbumRepositoryTest));
		
		/// <summary>
		/// 
		/// </summary>
		private IAlbumRepository repository;
		
		#region SetUp / TearDown
		[TestFixtureSetUp]
		public override void Init()
		{
			base.Init();
			
			repository = base.repositoryAlbum;
			Assert.IsNotNull(repository);
		}
		#endregion 
	}
}
