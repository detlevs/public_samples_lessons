﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 20.02.2010
 * Time: 18:18
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using DatabaseHelper;
using log4net;
using NHibernate;
using NHibernateMappingByCode;
using NHibernateMappingByCode.Repository;
using NUnit.Framework;

namespace NHibernateMappingByCodeTest
{
	//[TestFixture]
	public abstract partial class UnitTestBase
	{
		/// <summary>
		/// Logger.
		/// </summary>
		private static readonly ILog log = LogManager.GetLogger(typeof(UnitTestBase));
		
		/// <summary>
		/// Database Session 
		/// </summary>
		protected ISessionFactory sessionFactory;
		
		protected IArtistRepository repositoryArtist;
		
		protected IAlbumRepository repositoryAlbum;

		
		#region SetUp / TearDown
		[TestFixtureSetUp]
		virtual public void Init()
		{
			//Log4NetHelper.LoadLoggingConfig();
			log.Debug("TestFixtureSetUp");
			
			Assert.IsTrue(DatabaseManager.CopyDatabase());
			
			ApplicationContext.AppConfigure();

			// Use Real DB
			sessionFactory = ApplicationContext.SessionFactory;
			Assert.IsNotNull(sessionFactory);
			
			repositoryArtist = new ArtistRepository(sessionFactory);
			Assert.IsNotNull(repositoryArtist);
			
			repositoryAlbum = new AlbumRepository(sessionFactory);
			Assert.IsNotNull(repositoryAlbum);
		}
		
		[TestFixtureTearDown]
		virtual public void Dispose()
		{
			log.Debug("TestFixtureTearDown");
			Assert.IsTrue(DatabaseManager.DropDatabase());
		}
		
		[SetUp]
		virtual public void Setup()
		{
		}
		
		[TearDown]
		virtual public void TearDown()
		{
		}
		#endregion
	}
}
