﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 31.05.2012
 * Time: 22:19
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace NHibernateMappingByCodeTest.Helper
{
	/// <summary>
	/// Description of CustomDebugWriter.
	/// 
	/// http://geekswithblogs.net/lszk/archive/2011/07/12/showing-a-sql-generated-by-nhibernate-on-the-vs-build-in.aspx
	/// </summary>
	public class CustomDebugWriter : TextWriter
	{
		public override void WriteLine(string value)
        {
            Debug.WriteLine(value);
            base.WriteLine(value);
        }
		
        public override void Write(string value)
        {
            Debug.Write(value);
            base.Write(value);
        }
        
        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
	}
}
