﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 19.06.2012
 * Time: 23:38
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace MyBatis.Domain
{
	/// <summary>
	/// Description of ArtistAlbum.
	/// </summary>
	public class ArtistAlbum
	{
		public ArtistAlbum()
		{
		}
		
		#region Properties Artist
		public virtual int IdArtist { get; set; }
		public virtual string Name { get; set; }		
		#endregion
		
		#region Properties Album
		
		public virtual int IdAlbum { get; set; }
		public virtual string Title { get; set; }
		
		#endregion
	}
}
