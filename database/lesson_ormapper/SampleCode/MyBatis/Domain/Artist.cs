﻿/*
 * Created by SharpDevelop.
 * User: detlevs
 * Date: 19.06.2012
 * Time: 23:38
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace MyBatis.Domain
{
	/// <summary>
	/// Description of Artist.
	/// </summary>
	public class Artist
	{
		public Artist()
		{
		}
		
		#region Properties		
		public virtual int Id { get; set; }
		public virtual string Name { get; set; }		
		#endregion
	}
}
